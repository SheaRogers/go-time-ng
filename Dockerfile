FROM nginx:alpine

COPY ci/default.conf.template /etc/nginx/conf.d/default.conf.template
COPY ci/nginx.conf /etc/nginx/nginx.conf

WORKDIR /usr/share/nginx/html
COPY dist/go-time-ng/ .

CMD /bin/sh -c "envsubst '\$PORT' < /etc/nginx/conf.d/default.conf.template > /etc/nginx/conf.d/default.conf" && nginx -g 'daemon off;'
